with (import <nixpkgs> {});

stdenv.mkDerivation {
  name = "Manuel-de-philo";

  buildInputs = [
    (emacsWithPackages (epkgs: [ epkgs.org
                                 epkgs.org-ref ]))
  ];
}
