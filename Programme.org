
* Notions
** Le sujet :tl:
*** La conscience :tl:tes:ts:
*** La perception :tl:
*** L’inconscient :tl:tes:ts:
*** Autrui :tl:tes:
*** Le désir :tl:tes:ts:
*** L’existence et le temps :tl:
** La culture :tl:
*** Le langage :tl:tes:
*** L’art :tl:tes:ts:
*** Le travail et la technique :tl:tes:ts:
*** La religion :tl:tes:ts:
*** L’histoire :tl:tes:
** La raison et le réel :tl:
*** Théorie et expérience :tl:
*** La démonstration :tl:tes:ts:
*** L’interprétation :tl:tes:
*** Le vivant :tl:ts:
*** La matière et l’esprit :tl:tes:ts:
*** La vérité :tl:tes:ts:
** La politique :tl:
*** La société :tl:
*** La société et l'État :ts:
*** La société et les échanges :tes:
*** La justice et le droit :tl:tes:ts:
*** L’État :tl:tes:
** La morale :tl:
*** La liberté :tl:tes:ts:
*** Le devoir :tl:tes:ts:
*** Le bonheur :tl:tes:ts:

* Repères

 - Absolu/relatif
 - Abstrait/concret
 - En acte/en puissance
 - Analyse/synthèse
 - Cause/fin
 - Contingent/nécessaire/possible
 - Croire/savoir
 - Essentiel/accidentel
 - Expliquer/comprendre
 - En fait/en droit
 - Formel/matériel
 - Genre/espèce/individu
 - Idéal/réel
 - Identité/égalité/différence
 - Intuitif/discursif
 - Légal/légitime
 - Médiat/immédiat
 - Objectif/subjectif
 - Obligation/contrainte
 - Origine/fondement
 - Persuader/convaincre
 - Ressemblance/analogie
 - Principe/conséquence
 - En théorie/en pratique
 - Transcendant/immanent
 - Universel/général/particulier/singulier

* Auteurs

** Antiques

- Platon
- Aristote
- Épicure
- Lucrèce
- Sénèque
- Cicéron
- Épictète
- Marc Aurèle
- Sextus Empiricus
- Plotin
- Augustin
- Averroès
- Anselme
- Thomas d’Aquin
- Guillaume d’Ockham

** Classiques

- Machiavel
- Montaigne
- Bacon
- Hobbes
- Descartes
- Pascal
- Spinoza
- Locke
- Malebranche
- Leibniz
- Vico
- Berkeley
- Condillac
- Montesquieu
- Hume
- Rousseau
- Diderot
- Kant

** Modernes

- Hegel
- Schopenhauer
- Tocqueville
- Comte
- Cournot
- Mill
- Kierkegaard
- Marx
- Nietzsche
- Freud
- Durkheim
- Husserl
- Bergson
- Alain
- Russell
- Bachelard
- Heidegger
- Wittgenstein
- Popper
- Sartre
- Arendt
- Merleau-Ponty
- Levinas
- Foucault
