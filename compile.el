#!/usr/bin/env sh
":"; exec emacs --script "$0" -- "$@" # -*-emacs-lisp-*-

(defun kill-emacs (&rest _) (message "DO NOT DIE"))

(defun my-shell-command (cmd)
  (message "Running %s" cmd)
  (unless (zerop (shell-command cmd))
    (message "ERROR running %s"cmd)))

(require 'filenotify)
(require 'org)
(require 'org-ref)
(require 'ox-latex)

(setq org-latex-classes
      '(("book"
         "\\documentclass[11pt]{book}"
         ("\\part{%s}" . "\\part*{%s}")
         ("\\chapter{%s}" . "\\chapter*{%s}")
         ("\\section{%s}" . "\\section*{%s}")
         ("\\subsection{%s}" . "\\subsection*{%s}")
         ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
         ("\\paragraph{%s}" . "\\paragraph*{%s}")
         ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(find-file "Manuel.org")

(defun y-or-n-p (&rest _) t)
(defun yes-or-no-p (&rest _) t)

(defun my-cleanup nil
  (message "Cleaning up...")
  (mapc (lambda (f) (when (file-exists-p f) (delete-file f)))
	'("_output.aux"
	  "_output.bbl"
	  "_output.bcf"
	  "_output.blg"
	  "_output.idx"
	  "_output.ilg"
	  "_output.ind"
	  "_output.log"
	  "_output.out"
	  "_output.run.xml"
	  "_output.toc")))

(defun my-recompile (&rest _)
  (message "Recompiling...")

  (revert-buffer)
  (org-latex-export-to-latex)
  (org-html-export-to-html)

  (delete-file "_output.tex")
  (rename-file "Manuel.tex" "_output.tex")

  (my-shell-command "xelatex _output")
  (my-shell-command "biber _output")
  (my-shell-command "texindy _output.idx")
  (my-shell-command "xelatex _output")
  (my-shell-command "xelatex _output")

  (delete-file "Manuel.pdf")
  (rename-file "_output.pdf" "Manuel.pdf")
  (my-cleanup))

(my-cleanup)
(my-recompile)

(file-notify-add-watch "Manuel.org" '(change) 'my-recompile)
(file-notify-add-watch "preamble.tex" '(change) 'my-recompile)
(file-notify-add-watch "manuel.bib" '(change) 'my-recompile)
(message "Watching for changes...")
